package at.mnm.testing.services;

import java.util.concurrent.TimeoutException;

import com.google.common.base.Function;

public class ServiceWaitor<T> {

    private int maxWaitForServiceSuccessInMilliseconds;
    private T input;

    public ServiceWaitor(T input) {
        this(input, 5000);
    }

    /**
     * 
     * @param input3
     * @param maxWaitForServiceSuccessInMilliseconds
     */
    public ServiceWaitor(T input, int maxWaitForServiceSuccessInMilliseconds) {
        this.input = input;
        this.maxWaitForServiceSuccessInMilliseconds = maxWaitForServiceSuccessInMilliseconds;
    }

    /**
     * Very much based on WebDriver's FluentWait
     * 
     * @param testFunction
     *            <T,V>
     * @param pauseBetweenRetriesInMilliseconds
     * @return V
     */
    public <V> V until(Function<? super T, V> testFunction, int pauseBetweenRetriesInMilliseconds) {
        long startTime = System.currentTimeMillis();
        long maxTime = System.currentTimeMillis() + maxWaitForServiceSuccessInMilliseconds;
        while (true) {
            try {
                V value = testFunction.apply(input);
                if (value != null) {
                    return value;
                }

                long now = System.currentTimeMillis();
                if (now > maxTime) {
                    throw new TimeoutException("Waited for " + (now - startTime) + "milliseconds (" + input + ")");
                }

                Thread.sleep(pauseBetweenRetriesInMilliseconds);

            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Very much based on WebDriver's FluentWait
     * 
     * @param testFunction
     *            <T,V>
     * @return V
     */
    public <V> V until(Function<? super T, V> testFunction) {
        return until(testFunction, 100);
    }
}
