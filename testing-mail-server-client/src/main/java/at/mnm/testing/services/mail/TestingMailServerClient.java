package at.mnm.testing.services.mail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestingMailServerClient {

    private static final Class<TestingMailServerClient> THIS_CLASS = TestingMailServerClient.class;
    private static final String TESTING_MAIL_SERVER_BASE_URL = "testingMailServer.baseUrl";
    private String baseUrl;
    private RestTemplate restTemplate;
    private List<Link> searchLinks;

    public TestingMailServerClient(String baseUrl) {
        this.baseUrl = baseUrl;
        restTemplate = setupNewRestTemplate();
        searchLinks = requestSearchLinks(baseUrl);
    }

    public void deleteMessage(long messageId) {
        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put("messageId", messageId);
        restTemplate.delete(baseUrl + "/{messageId}", urlVariables);
    }

    public List<Message> findByMessageTextContent(String textToLookFor) {

        Link searchLink = findLinkFor("findByMessageTextContent");
        UriTemplate uriTemplate = new UriTemplate(searchLink.getHref());

        Map<String, Object> urlVariables = new HashMap<String, Object>();
        urlVariables.put(uriTemplate.getVariableNames().get(0), textToLookFor);

        ResponseEntity<PagedResources<Message>> responseEntity = restTemplate.exchange(
                uriTemplate.expand(urlVariables), HttpMethod.GET, null,
                new ParameterizedTypeReference<PagedResources<Message>>() {
                });

        PagedResources<Message> pagedResources = responseEntity.getBody();

        return new ArrayList<Message>(pagedResources.getContent());

    }

    private Link findLinkFor(String linkName) {
        for (Link link : searchLinks) {
            if (link.getRel().equals(linkName)) {
                return link;
            }
        }
        return null;
    }

    protected List<Link> requestSearchLinks(String baseUrl2) {
        @SuppressWarnings("unchecked")
        PagedResources<Message> pagingInformation = restTemplate
                .getForObject(baseUrl + "/search", PagedResources.class);
        return pagingInformation.getLinks();
    }

    private RestTemplate setupNewRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        ObjectMapper mapper = createJacksonObjectMapper();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(mapper);

        restTemplate.getMessageConverters().clear();
        restTemplate.getMessageConverters().add(converter);

        return restTemplate;
    }

    private ObjectMapper createJacksonObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new Jackson2HalModule());
        return mapper;
    }

    public static void main(String[] args) {

        String baseUrl = System.getProperty(TESTING_MAIL_SERVER_BASE_URL);

        if (args.length == 0 || (args.length <= 1 && baseUrl == null)) {
            exitWithUsage();
        }

        List<String> searchStrings = Arrays.asList(args);
        if (baseUrl == null) {
            baseUrl = searchStrings.remove(0);
        }

        if (searchStrings.isEmpty()) {
            exitWithUsage();
        }

        TestingMailServerClient testingMailServerClient = new TestingMailServerClient(baseUrl);

        for (String searchString : searchStrings) {
            System.out.println("Search Results for '" + searchString + "'");
            System.out.println(testingMailServerClient.findByMessageTextContent(searchString));
        }

        // deletion example:
        //
        // testingMailServerClient.deleteMessage(1);

    }

    private static void exitWithUsage() {
        o("Usage " + THIS_CLASS.getName() + " {baseUrl} searchString... ");
        o("      search the testing mail servers messages' bodies for searchstring");
        o("+   baseUrl is the URL that will return the main messages resource page");
        o("    e.g. http://localhost:28080/messages");
        o("+   searchString is used to search for messages; at least one searchString");
        o("    is required and can contain wild cards");
        o("    e.g. %expected message text%");
        o("+   When the Java system property 'testingMailServer.baseUrl' IS set, the ");
        o("    first argument is always interpretated as a searchstring.");
        o("+   When the Java system property 'testingMailServer.baseUrl' IS NOT set,");
        o("    the first argument is always interpretated as a baseUrl.");
        System.exit(-1);
    }

    private static void o(String text) {
        System.err.println(text);
    }

}
