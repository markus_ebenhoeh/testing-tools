package at.mnm.testing.services.mail;

import java.util.List;

import org.springframework.hateoas.Link;

public class Message {

    private long id;
    private String messageText;
    private String sender;
    private String recipient;
    private String subject;
    private Link link;
    private List<MessageHeader> messageHeaders;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getMessageText() {
        return messageText;
    }
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
    public String getSender() {
        return sender;
    }
    public void setSender(String sender) {
        this.sender = sender;
    }
    public String getRecipient() {
        return recipient;
    }
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public List<MessageHeader> getMessageHeaders() {
        return messageHeaders;
    }
    public void setMessageHeaders(List<MessageHeader> messageHeaders) {
        this.messageHeaders = messageHeaders;
    }
    
    public Link getLink() {
        return link;
    }
    public void setLink(Link link) {
        this.link = link;
    }
    @Override
    public String toString() {
        return "Message [id=" + id + ", messageText=" + messageText + ", sender=" + sender + ", recipient=" + recipient
                + ", subject=" + subject + ", messageHeaders=" + messageHeaders + "]";
    }
    

}
