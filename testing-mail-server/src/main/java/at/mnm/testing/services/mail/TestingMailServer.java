package at.mnm.testing.services.mail;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;

public class TestingMailServer extends Thread {

    private static final Logger log = LoggerFactory.getLogger(TestingMailServer.class);

    private SimpleSmtpServer server;
    private int port;
    private MessageMapper messageMapper;
    private MessageRestRepository messageRestRepository;

    @Autowired
    public TestingMailServer(int port, MessageMapper messageMapper, MessageRestRepository messageRestRepository) {
        super("TestingMailServer");
        this.port = port;
        this.messageMapper = messageMapper;
        this.messageRestRepository = messageRestRepository;
    }

    @Override
    public void run() {
        try {

            server = start(port);

            storeProcessedMessagesUntilServerIsStopped();

        } catch (Exception e) {

            log.info("Stopping mail server because:", e);

            server.stop();
        }

    }

    public SimpleSmtpServer start(int port) {
        SimpleSmtpServer server = new SimpleSmtpServer(port);
        Thread t = new Thread(server, "SimpleSmtpServer");
        t.start();

        synchronized (server) {
            try {
                server.wait();
            } catch (InterruptedException e) {
            }
        }
        return server;
    }

    private void storeProcessedMessagesUntilServerIsStopped() throws InterruptedException {
        while (!server.isStopped() || server.getReceivedEmailSize() > 0) {
            saveMessages();
            Thread.sleep(50);
        }
    }

    private void saveMessages() {
        @SuppressWarnings("unchecked")
        Iterator<SmtpMessage> receivedEmailIterator = server.getReceivedEmail();

        if (receivedEmailIterator.hasNext()) {
            SmtpMessage smtpmessage = receivedEmailIterator.next();
            receivedEmailIterator.remove();

            Message message = messageMapper.convertTo(smtpmessage);
            messageRestRepository.save(message);
        }

    }

}
