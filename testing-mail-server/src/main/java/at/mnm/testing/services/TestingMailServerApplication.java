package at.mnm.testing.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import at.mnm.testing.services.mail.Message;
import at.mnm.testing.services.mail.MessageHeader;
import at.mnm.testing.services.mail.MessageMapper;
import at.mnm.testing.services.mail.MessageRestRepository;
import at.mnm.testing.services.mail.TestingMailServer;

@Configuration
@ComponentScan
@EnableJpaRepositories
@Import(CustomRestMvcConfiguration.class)
@EnableAutoConfiguration
public class TestingMailServerApplication {

    private static final String TESTING_MAIL_SERVER = "testingMailServer";
    private static final String TESTING_MAIL_SERVER_HTTP_PORT = TESTING_MAIL_SERVER + ".http.port";
    private static final String TESTING_MAIL_SERVER_SMTP_PORT = TESTING_MAIL_SERVER + ".smtp.port";
    private static final Class<TestingMailServerApplication> THIS_CLASS = TestingMailServerApplication.class;

    public static void main(String[] args) {

        Settings testingMailServerSettings = checkSettingsOrBailOut();

        System.setProperty("server.port", testingMailServerSettings.httpPort.toString());

        ConfigurableApplicationContext context = SpringApplication.run(TestingMailServerApplication.class);
        MessageRestRepository repository = context.getBean(MessageRestRepository.class);

        repository.save(new Message("This is test message A", "afro@m.es", "tob@m.es", "Some Subject", someHeaders()));
        repository.save(new Message("This is test message B", "bfro@m.es", "toc@m.es", "A Subject", someHeaders()));
        repository.save(new Message("This is test message C", "cfro@m.es", "tod@m.es", "Another Subject",
                someHeaders()));

        TestingMailServer testingMailServer = new TestingMailServer(testingMailServerSettings.smtpPort,
                new MessageMapper(), repository);

        testingMailServer.start();
        System.out.println("SMTP server is listening on : " + testingMailServerSettings.smtpPort);
        System.out.println("HTTP server is listening on : " + testingMailServerSettings.httpPort);

    }

    private static Settings checkSettingsOrBailOut() {

        Settings testingMailServerSettings = new Settings();

        testingMailServerSettings.smtpPort = Integer.getInteger(TESTING_MAIL_SERVER_SMTP_PORT);
        testingMailServerSettings.httpPort = Integer.getInteger(TESTING_MAIL_SERVER_HTTP_PORT);

        if (testingMailServerSettings.smtpPort == null || testingMailServerSettings.httpPort == null) {
            exitWithUsage();
        }

        return testingMailServerSettings;
    }

    private static List<MessageHeader> someHeaders() {
        List<MessageHeader> headers = new ArrayList<>();
        headers.add(new MessageHeader("header1", "value1"));
        headers.add(new MessageHeader("header2", "value2"));
        headers.add(new MessageHeader("header3", "value3"));
        return headers;
    }

    private static void exitWithUsage() {
        o("Usage " + THIS_CLASS.getName());
        o("    A testing mail servers that can receive mails and exposes them");
        o("    via a RESTful hal+json API.");
        o("Required system properties:");
        o("+   " + TESTING_MAIL_SERVER_SMTP_PORT + " the port the SMTP server should listen to, e.g. 28025");
        o("+   " + TESTING_MAIL_SERVER_HTTP_PORT + " the port the HTTP server should listen to, e.g. 28080");
        System.exit(-1);
    }

    private static class Settings {
        Integer smtpPort = 0;
        Integer httpPort = 0;
    }

    private static void o(String text) {
        System.err.println(text);
    }

}
