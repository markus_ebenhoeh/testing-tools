package at.mnm.testing.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import at.mnm.testing.services.mail.MessageValidator;

@Configuration
public class CustomRestMvcConfiguration extends RepositoryRestMvcConfiguration {

    
    @Bean
    public LocalValidatorFactoryBean getLocalValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }
    
    
    @Override
    protected void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener v) {
        MessageValidator messageValidator = new MessageValidator();
        v.addValidator("beforeCreate", messageValidator);
        v.addValidator("beforeCreate", getLocalValidatorFactoryBean());
        v.addValidator("beforeSave", messageValidator);
        
    }
}
