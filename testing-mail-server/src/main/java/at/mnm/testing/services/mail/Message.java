package at.mnm.testing.services.mail;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Size(max = 640000)
    private String messageText;

    @Size(max = 255)
    private String sender;

    @Size(max = 255)
    private String recipient;

    @Size(max = 255)
    private String subject;

    @OneToMany(cascade = { CascadeType.PERSIST })
    @Cascade({ org.hibernate.annotations.CascadeType.ALL })
    @JoinColumn(name = "message_id", nullable = false)
    @JsonManagedReference
    private List<MessageHeader> messageHeaders;
    
    public Message() {
    }
    

    public Message(String messageText, String sender, String recipient, String subject,
            List<MessageHeader> messageHeaderList) {
        this.messageText = messageText;
        this.sender = sender;
        this.recipient = recipient;
        this.subject = subject;
        this.messageHeaders = messageHeaderList;
        setMesageHeaderParentmessage(messageHeaders);
    }

    private void setMesageHeaderParentmessage(List<MessageHeader> messageHeaders) {
        for (MessageHeader messageHeader : messageHeaders) {
            messageHeader.setMessage(this);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public List<MessageHeader> getMessageHeaders() {
        return messageHeaders;
    }

    public void setMessageHeaders(List<MessageHeader> messageHeaders) {
        this.messageHeaders = messageHeaders;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
