package at.mnm.testing.services.mail;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "messages", path = "messages")
public interface MessageRestRepository extends PagingAndSortingRepository<Message, Long> {

    List<Message> findByRecipient(@Param("recipient") String recipient);

    @Query("SELECT m FROM Message m WHERE m.messageText  like :messageSubText")
    List<Message> findByMessageTextContent(@Param("messageSubText") String messageSubText);
}
