package at.mnm.testing.services.mail;

import static org.springframework.util.StringUtils.hasLength;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class MessageValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Message.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        if (! (target instanceof Message)) {
            errors.reject("What is this?");
        } else {
            Message message = (Message) target;
            
            if (!hasLength(message.getMessageText())) {
                errors.rejectValue("messageText", "message.messagetext.mandatory");
            }

        }

    }

}
