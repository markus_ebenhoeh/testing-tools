package at.mnm.testing.services.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

@RepositoryEventHandler(Message.class)
public class MessageEventhandler {
    
    final static Logger logger = LoggerFactory.getLogger(MessageEventhandler.class);

    @HandleBeforeSave
    public void handleBeforeSavingMessageEvent(Message p) {
        
        logger.debug("handleMessageSave --- NOT CALLED, WHYYYYYYYYYYYY!!");
        
    }



}