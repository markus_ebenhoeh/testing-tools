package at.mnm.testing.services.mail;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dumbster.smtp.SmtpMessage;

@Component
public class MessageMapper {

    public Message convertTo(SmtpMessage smtpMessage) {
        List<MessageHeader> allHeaders = allHeaders(smtpMessage);
        return new Message(smtpMessage.getBody(), findFirstFromSender(allHeaders), findFirstRecepient(allHeaders),
                findFirstSubject(allHeaders), allHeaders);
    }

    private String findFirstSubject(List<MessageHeader> allHeaders) {
        return findHeader(allHeaders, "Subject");
    }

    private String findFirstRecepient(List<MessageHeader> allHeaders) {
        return findHeader(allHeaders, "To");
    }

    private String findFirstFromSender(List<MessageHeader> allHeaders) {
        return findHeader(allHeaders, "From");
    }

    private String findHeader(List<MessageHeader> allHeaders, String nameToSearchFor) {
        for (MessageHeader messageHeader : allHeaders) {
            if (messageHeader.getName().equalsIgnoreCase(nameToSearchFor)) {
                return messageHeader.getValue();
            }
        }
        return null;
    }

    private List<MessageHeader> allHeaders(SmtpMessage smtpMessage) {

        @SuppressWarnings("unchecked")
        Iterator<String> headerNames = smtpMessage.getHeaderNames();

        List<MessageHeader> headersList = new ArrayList<>();

        while (headerNames.hasNext()) {
            String headerName = headerNames.next();
            for (String headerValue : smtpMessage.getHeaderValues(headerName)) {
                headersList.add(new MessageHeader(headerName, headerValue));
            }
        }

        return headersList;
    }

}
