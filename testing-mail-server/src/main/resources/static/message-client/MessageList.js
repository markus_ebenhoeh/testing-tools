define(function(require) {
   var MessageModel = require('./MessageModel');
   var HAL = require('HAL');

   return HAL.Collection.extend({
      model : MessageModel,
      url : '/messages',
      itemRel : 'messages',

      initialize : function() {
         _.bindAll(this, 'nextMessageModel', 'previousMessageModel');
      },

      nextMessageModel: function(messageModel) {
         var index = this.indexOf(messageModel);
         if ((index + 1) === this.length) {
            return this.first();
         }
         return this.at(index + 1);
      },

      previousMessageModel : function(messageModel) {
         var index = this.indexOf(messageModel);
         if (index === 0) {
            return this.last();
         }
         return this.at(index - 1);
      }      
      
   });

});
