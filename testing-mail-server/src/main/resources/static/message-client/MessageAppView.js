

define(function(require) {
	var backbone = require('backbone');
	var $ = require('jquery');

	return backbone.View.extend({
		
		
		el : $("#message-app"),
		
	    events: {
	        "click .resetMessageForm": "resetForm",
	        "click .addNew": "createNew",
	      },
	      
		
      initialize: function() {
	      this.editMessageText = this.$("#messageForm #editMessageText");
	      this.editMessageSource = this.$("#messageForm #editMessageSource");
	      this.editMessageTime = this.$("#messageForm #editMessageTime");
	      this.editMessageSubmitter = this.$("#messageForm #editMessageSubmitter");
	      
      },
	  
	  createNew: function() {
		  this.model.create({
        	  messageText: this.editMessageText.val(),
        	  messageSource: this.editMessageSource.val(),
        	  firstDocumentedOccurence: this.editMessageTime.val(),
        	  submitter: this.editMessageSubmitter.val(),
          });
		  this.resetForm();
      },
	  
      resetForm: function(todo) {
	      this.editMessageText.val('');
	      this.editMessageSource.val('');
	      this.editMessageTime.val('');
	      this.editMessageSubmitter.val('');
      }
	    
	});
	
	
	
	
});
