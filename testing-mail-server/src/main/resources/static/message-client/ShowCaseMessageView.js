define(function(require) {
   var backbone = require('backbone');
   var $ = require('jquery');
   var _ = require('underscore');

   return backbone.View.extend({

      el : $("#message-app"),

      currentMessageModel : null,

      initialize : function() {
         this.listenTo(this.model, 'add', this.change);
         this.listenTo(this.model, 'reset', this.change);
         this.listenTo(this.model, 'all', this.change);
         this.template = _.template($('#message-showcase-template').html());
      },

      showNextMessage : function() {
         var lastMessageModel = this.currentMessageModel;
         if (this.model.length === 0) {
            this.currentMessageModel = null;
         } else {
            if (this.currentMessageModel == null) {
               this.currentMessageModel = this.model.first();
            } else {
               this.currentMessageModel = this.model.nextMessageModel(this.currentMessageModel);
            }

            if (this.currentMessageModel !== null) {
               this.renderMessageModel(lastMessageModel, this.currentMessageModel);
               setTimeout(_.bind(this.showNextMessage, this), 8000);
            }
         }
      },

      renderMessageModel : function(lastMessageModel, newMessageModel) {

         var newMessageHtml = this.template(newMessageModel.attributes);
         this.$(".presentation").fadeOut({
            duration : 2000
         }).promise().done(function() {
            $(".presentation").html(newMessageHtml);
            if (lastMessageModel !== null) {
               $(".presentation").removeClass("font" + lastMessageModel.id);
            }
            $(".presentation").addClass("font" + newMessageModel.id);
            $(".presentation").fadeIn({
               duration : 2000
            });
         });
      },

      change : function() {
         if (this.currentMessageModel === null && this.model.length > 0) {
            this.showNextMessage();
         }
      }

   });

});
