define(function (require) {
    var $ = require('jquery');

	$(document).ready(function() {
		var MessageList = require('./MessageList');
		var MessageListView = require('./MessageListView');
		var MessageAppView = require('./MessageAppView');
		var ShowCaseMessageView = require('./ShowCaseMessageView');

		var messageList= new MessageList();
		new MessageListView({model:messageList});
		new MessageAppView({model:messageList});
		new ShowCaseMessageView({model:messageList});;
	});    
    
});

