

requirejs.config({
    baseUrl: 'lib',
    paths: {
        messageclient: '../message-client',
        jquery: 'jquery/jquery.min',
        backbone: 'backbone-amd/backbone',
        Backbone: 'backbone-amd/backbone',
        underscore: 'lodash/lodash.min',
        HAL: 'HAL/backbone-hal'
    }
});
