# Testing Mail Server

## What it can do

Starts an SMTP server on a given port and an HTTP server on another and allows
checking received e-mails.

All Mails for all addresses are kept in an in-memory DB  and access is not restricted.

Mails that are marked as read are deleted.


## What it cannot do and will never do

- persistence of the messages
- authentication
- mail forwarding (thought this might be useful if  the testing involves checking the mail in different mail readers.)



